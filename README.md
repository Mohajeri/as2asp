Agent script to ASP (clingo) translation for ECAI2020
run: 
```
$  clingo -c maxtime=100 -n 0 dec.lp match.lp [match_0.lp|match_1.lp|match_2.lp]
```